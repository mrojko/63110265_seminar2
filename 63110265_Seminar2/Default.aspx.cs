﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63110265_Seminar2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var websr = new ServiceReference1.HRMServiceClient();
            if (!IsPostBack) {
                var poizvedba = (from x in websr.GetDepartmentList()
                                 select x.DepartmentGroupName).Distinct();
                DDL_DepartmentGroup.DataSource = poizvedba;
                DDL_DepartmentGroup.DataBind();
                var poizvedba2 = (from x in websr.GetDepartmentList()
                                  where x.DepartmentGroupName == DDL_DepartmentGroup.SelectedValue
                                  select new {x.DepartmentName, x.DepartmentID}).Distinct();
                DDL_Department.DataSource = poizvedba2.ToList();
                DDL_Department.DataValueField = "DepartmentID";
                DDL_Department.DataTextField = "DepartmentName";
                DDL_Department.DataBind();

                
            }
            else if (IsPostBack)
            {
            }

        }

        protected void DDL_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            reset_department();
        }

        protected void DDL_DepartmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            reset_departmentgroup();

            var websr = new ServiceReference1.HRMServiceClient();
            var poizvedba2 = (from x in websr.GetDepartmentList()
                              where x.DepartmentGroupName == DDL_DepartmentGroup.SelectedValue
                              select new { x.DepartmentName, x.DepartmentID }).Distinct();
            DDL_Department.DataSource = poizvedba2.ToList();
            DDL_Department.DataValueField = "DepartmentID";
            DDL_Department.DataTextField = "DepartmentName";
            DDL_Department.DataBind();
            DDL_Department_SelectedIndexChanged(sender, e);
        }


        protected void GV_Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GV_Employee.SelectedIndex != -1 && DDL_Department.SelectedItem.ToString() == "Sales")
                B_Orders.Visible = true;
            else
                B_Orders.Visible = false;
            DV_EmployeeDetails.Visible = true;
            // prikazi details view
            var websr = new ServiceReference1.HRMServiceClient();
            DV_EmployeeDetails.Visible = true;
            L_SaleStatistics1.Text = GV_Employee.SelectedDataKey.Value.ToString();
            var poizvedba = (from x in websr.GetEmployeeList()
                             where x.EmployeeID == Convert.ToInt32(GV_Employee.SelectedDataKey.Value)
                             select new
                             {
                                 Ime_in_priimek = x.FirstName + " " + x.LastName,
                                 Spol = x.Gender,
                                 Datum_rojstva = x.BirthDate,
                                 Naslov1 = x.Address ,
                                 Naslov2 = x.City + " " + x.PostalCode, 
                                 Naslov3 = x.State,
                                 Naslov4 = x.Country,
                                 Telefonska_številka = x.PhoneNumber,
                                 Elektronski_naslov = x.EmailAddress,
                                 Datum_zaposlitve = x.HireDate,
                                 Delovna_doba = DateTime.Now.Subtract(x.HireDate).Days / 356 + " let",
                                 Delovno_mesto = x.JobTitle,
                                 Izmena = x.Shift + " (" + x.ShiftStartTime + " - " + x.ShiftEndTime + ")",
                                 Urna_Postavka = x.PayRate
                             }).ToList();
            DV_EmployeeDetails.DataSource = poizvedba;
            DV_EmployeeDetails.DataBind();
            hide_orders();
        }

        protected void GV_Employee_PageIndexChanged(object sender, EventArgs e)
        {
            reset_departmentgroup();
        }

        protected void B_Orders_Click(object sender, EventArgs e)
        {
            show_orders();
            
            var websr = new ServiceReference1.HRMServiceClient();
            var poizvedba = (from x in websr.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue))
                             select x.Customer);
            L_SaleStatistics1.Text = poizvedba.Distinct().Count().ToString();
            if (poizvedba.Distinct().Count() == 0)
                GV_Orders.Visible = false;
            L_SaleStatistics2.Text = poizvedba.Count().ToString();
            var poizvedba1 = (from x in websr.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue))
                              select new { Lol = x.SubTotal }
                             ).AsEnumerable().Sum(x => x.Lol);
            L_SaleStatistics3.Text = string.Format("{0:c}", poizvedba1);
            GV_Orders.DataBind();
        }

        void show_orders() {
            L_SaleStatistics1.Visible = true;
            L_SaleStatistics2.Visible = true;
            L_SaleStatistics3.Visible = true;
            Pre_SaleStatistics1.Visible = true;
            Pre_SaleStatistics2.Visible = true;
            Pre_SaleStatistics3.Visible = true;
            GV_Orders.Visible = true;
        }

        void hide_orders() {
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            Pre_SaleStatistics1.Visible = false;
            Pre_SaleStatistics2.Visible = false;
            Pre_SaleStatistics3.Visible = false;
            GV_Orders.Visible = false;
        }

        void reset_departmentgroup() {
            B_Orders.Visible = false;
            GV_Employee.SelectedIndex = -1;
            DV_EmployeeDetails.Visible = false;
            hide_orders();
        }

        void reset_department() {
            GV_Employee.PageIndex = 0;
            reset_departmentgroup();
        }

        protected void GV_Orders_DataBound(object sender, EventArgs e)
        {

        }

        protected void B_Find_Click(object sender, EventArgs e)
        {
            
        }

    }
}