﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63110265_Seminar2.Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="XX-Large" Text="HRM"></asp:Label>
        <br />
    
        <hr />
        <asp:Label ID="L_DepartmentGroup" runat="server" Text="Organizacijska enota" Font-Bold="True" Font-Names="Times New Roman"></asp:Label>
&nbsp;
        <asp:DropDownList ID="DDL_DepartmentGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_DepartmentGroup_SelectedIndexChanged">
        </asp:DropDownList>
        <br />
        <asp:Label ID="L_Deparment" runat="server" Text="Oddelek" Font-Bold="True" Font-Names="Times New Roman"></asp:Label>
&nbsp;
        <asp:DropDownList ID="DDL_Department" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_Department_SelectedIndexChanged">
        </asp:DropDownList>
        <hr />
        <br />
        <asp:GridView ID="GV_Employee" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="EmployeeID" DataSourceID="ObjectDataSource1" ForeColor="Black" GridLines="Horizontal" OnPageIndexChanged="GV_Employee_PageIndexChanged" OnSelectedIndexChanged="GV_Employee_SelectedIndexChanged" PageSize="5">
            <Columns>
                <asp:TemplateField HeaderText="Ime in priimek">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                        &nbsp;<asp:Label ID="Label2" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="JobTitle" HeaderText="Delovno mesto" SortExpression="JobTitle" />
                <asp:BoundField DataField="HireDate" DataFormatString="{0:d}" HeaderText="Datum zaposlitve" SortExpression="HireDate" />
                <asp:CommandField ShowSelectButton="True" SelectText="Podrobnosti" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" BorderWidth="0px" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerSettings Mode="NextPrevious" />
            <PagerStyle BackColor="#333333" ForeColor="White" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="False" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetEmployeeListForDepartment" TypeName="_63110265_Seminar2.ServiceReference1.HRMServiceClient">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Department" Name="departmentID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:DetailsView ID="DV_EmployeeDetails" runat="server" Height="50px" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateRows="False" style="margin-right: 0px" FooterText=" " HeaderText=" ">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="White" Font-Bold="True" />
            <FieldHeaderStyle BackColor="#999999" Font-Bold="True" ForeColor="Black" />
            <Fields>
                <asp:BoundField DataField="Ime_in_priimek" HeaderText="Ime in priimek:" />
                <asp:BoundField DataField="Spol" HeaderText="Spol:" />
                <asp:BoundField DataField="Datum_rojstva" HeaderText="Datum rojstva:" DataFormatString="{0:d}" />
                <asp:TemplateField></asp:TemplateField>
                <asp:BoundField DataField="Naslov1" HeaderText="Naslov:">
                <FooterStyle BorderStyle="None" />
                </asp:BoundField>
                <asp:BoundField DataField="Naslov2" />
                <asp:BoundField DataField="Naslov3" />
                <asp:BoundField DataField="Naslov4" />
                <asp:TemplateField></asp:TemplateField>
                <asp:BoundField DataField="Telefonska_številka" HeaderText="Telefonska številka:" />
                <asp:BoundField DataField="Elektronski_naslov" HeaderText="Elektronski naslov:" />
                <asp:TemplateField></asp:TemplateField>
                <asp:BoundField DataField="Datum_zaposlitve" HeaderText="Datum zaposlitve:" />
                <asp:BoundField DataField="Delovna_doba" HeaderText="Delovna doba:" />
                <asp:BoundField DataField="Delovno_mesto" HeaderText="Delovno mesto:" />
                <asp:BoundField DataField="Izmena" HeaderText="Izmena:" />
                <asp:TemplateField></asp:TemplateField>
                <asp:BoundField DataField="Urna_postavka" HeaderText="Urna postavka:" DataFormatString="{0:c}" />
            </Fields>
            <FooterStyle BackColor="#333333" ForeColor="White" Font-Bold="True" Height="30px" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" Height="30px" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#333333" />
        </asp:DetailsView>
        <hr />
        <br />
        <asp:Button ID="B_Orders" runat="server" Text="Prikaži ustvarjen promet" OnClick="B_Orders_Click" Visible="False" />
        <br />
        <br />
        <asp:Label ID="Pre_SaleStatistics1" runat="server" Text="Število strank: " Font-Bold="True" Visible="False"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="L_SaleStatistics1" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="Pre_SaleStatistics2" runat="server" Text="Število naročil" Font-Bold="True" Visible="False"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="L_SaleStatistics2" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="Pre_SaleStatistics3" runat="server" Text="Skupna vrednost naročil" Font-Bold="True" Visible="False"></asp:Label>
&nbsp;&nbsp;
        <asp:Label ID="L_SaleStatistics3" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Orders" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="ObjectDataSource2" ForeColor="Black" GridLines="Horizontal" Visible="False" DataKeyNames="Customer" OnDataBound="GV_Orders_DataBound" AllowPaging="True" PageSize="20">
            <Columns>
                <asp:BoundField DataField="OrderNumber" HeaderText="Naročilo" SortExpression="OrderNumber" />
                <asp:BoundField DataField="OrderDate" DataFormatString="{0:d}" HeaderText="Datum" SortExpression="OrderDate" />
                <asp:BoundField DataField="Customer" HeaderText="Stranka" SortExpression="Customer" />
                <asp:BoundField DataField="Amount" HeaderText="Število izdelkov" SortExpression="Amount" />
                <asp:BoundField DataField="SubTotal" DataFormatString="{0:c}" HeaderText="Vrednost" SortExpression="SubTotal" />
                <asp:BoundField DataField="TaxAmt" DataFormatString="{0:c}" HeaderText="Davek" SortExpression="TaxAmt" />
                <asp:BoundField DataField="Freight" DataFormatString="{0:c}" HeaderText="Dostava" SortExpression="Freight" />
                <asp:BoundField DataField="TotalDue" DataFormatString="{0:c}" HeaderText="Za plačilo" SortExpression="TotalDue" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetOrderList" TypeName="_63110265_Seminar2.ServiceReference1.HRMServiceClient">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Employee" Name="employeeID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <br />
        </div>
    </form>
</body>
</html>
